export class ChatMessage {
  type: string;
  message: string;
  reply: boolean;
  sender: string;
  avatar: string;


  constructor(type: string, message: string, reply: boolean, sender: string, avatar: string) {
    this.type = type;
    this.message = message;
    this.reply = reply;
    this.sender = sender;
    this.avatar = avatar;
  }
}
