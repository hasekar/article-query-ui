import { Injectable } from '@angular/core';
import {ChatMessage} from '../entities/ChatMessage';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ChatbotService {

  starterMessage: ChatMessage = new ChatMessage('text',
    'Welcome to KE Article query Bot. Please type your query string',
    false,
    'Bot',
    '');

  messages: ChatMessage[] = [];

  constructor(private httpClient: HttpClient) {
    this.messages.push(this.starterMessage);
  }

  postArticleQuery(message: string): Observable<ChatMessage> {
    return this.httpClient.post<ChatMessage>('http://127.0.0.1:5000/query-article', { keyword: message }, httpOptions);
  }
}
