import { Component, OnInit } from '@angular/core';
import {ChatMessage} from '../entities/ChatMessage';
import {ChatbotService} from '../service/chatbot.service';

@Component({
  selector: 'app-chatbot-python',
  templateUrl: './chatbot-python.component.html',
  styleUrls: ['./chatbot-python.component.css']
})
export class ChatbotPythonComponent implements OnInit {

  noMessage: ChatMessage = new ChatMessage('text',
    'Could not find text with the entered Keyword',
    false,
    'Bot',
    '');

  messages: ChatMessage[] = [];
  dateNow: Date;
  constructor(protected chatService: ChatbotService) {
    this.messages = chatService.messages;
  }
  ngOnInit(): void {
  }

  sendMessage(event: any) {
    this.messages.push(new ChatMessage('text', event.message, true, 'John', ''));
    this.chatService.postArticleQuery(event.message).subscribe(messageInput => {
      console.log(messageInput);
      this.dateNow = new Date();
      if (messageInput.message === '') {
        this.messages.push(this.noMessage);
      } else {
        this.messages.push(messageInput);
      }
    }, error1 => {
      console.log(error1);
    });
  }

}
