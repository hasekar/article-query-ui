import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatbotPythonComponent } from './chatbot-python.component';

describe('ChatbotPythonComponent', () => {
  let component: ChatbotPythonComponent;
  let fixture: ComponentFixture<ChatbotPythonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatbotPythonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotPythonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
