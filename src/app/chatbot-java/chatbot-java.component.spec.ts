import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatbotJavaComponent } from './chatbot-java.component';

describe('ChatbotJavaComponent', () => {
  let component: ChatbotJavaComponent;
  let fixture: ComponentFixture<ChatbotJavaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatbotJavaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotJavaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
